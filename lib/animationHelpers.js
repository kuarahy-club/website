import { blockHeight } from '../config'

// simplifica la asignación de la distancia del scroll
// además de aplicar un smooth scroll
// mediante el uso de bloques
export function bh (blockLength) {
  return blockLength * blockHeight
}

// return distancia en pixeles del alto de la ventana según el porcentaje pasado
export function vh (percentage) {
  let innerHeight = 700
  if (typeof window !== "undefined") {
    innerHeight = window.innerHeight
  }
  return percentage * innerHeight / 100
}

// return distancia en pixeles del ancho de la ventana según el porcentaje pasado
export function vw (percentage) {
  let innerWidth = 700
  if (typeof window !== "undefined") {
    innerWidth = window.innerWidth
  }
  return percentage * innerWidth / 100
}