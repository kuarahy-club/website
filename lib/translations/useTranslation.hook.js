import { useContext } from 'react'
import { LocaleContext } from './locale.context'
import config from '../../config'
import get from 'lodash.get'
import { readLocaleStrings, requireBlogCategoryIndexFile, requireBlogPostContent } from './readLocaleStrings'
import _, { includes } from 'lodash'


export default function useTranslation() {
  const { strings, locale } = useContext(LocaleContext)

  function t(key) {
    const gettedString = get(strings, `${locale}.${key}`)
    if(!gettedString) {
      // console.warn(`Translation '${key}' for locale '${locale}' not found. Using default locale instead`)
    }
    return gettedString || get(strings, `${config.defaultLocale}.${key}`)
  }

  function getLocaledUrl(defaultUrl) {
    const gettedString = findLocaledUrlInString(defaultUrl, locale, strings)
    return gettedString
  }

  function getDefaultUrl(localedUrl) {
    const gettedString = findDefaultUrlInString(localedUrl, locale, strings)
    return gettedString
  }

  function replaceInPath(currentPath, fromLocale, toLocale) {
    // para cada ruta de página en el idioma locale, reemplazar el nombre en e.target.value
    strings[config.defaultLocale].pages.paths.forEach(pagePaths => {
      const regex = new RegExp(`\/(${pagePaths[fromLocale]})`)
      currentPath = currentPath.replace(regex, `/${pagePaths[toLocale]}`)
    })
    return currentPath
  }

  return {
    t,
    getLocaledUrl,
    getDefaultUrl,
    replaceInPath,
    locale
  }
}

//////////////////////////////////////
// LOCALIZED URL FOR SINGLE PAGES
//////////////////////////////////////

// obtiene el path traducido de páginas individuales alojadas en /locales
export function getLocaledUrlByLocale(defaultUrl, locale) {
  const strings = readLocaleStrings(locale, ['common'])
  const gettedString = findLocaledUrlInString(defaultUrl, locale, strings)
  return gettedString
}


export function getDefaultUrlByLocale(localedUrl, locale) {
  const strings = readLocaleStrings(locale, ['common'])
  const gettedString = findDefaultUrlInString(localedUrl, locale, strings)
  return gettedString
}

// Traduce la url desde defaultUrl de páginas alojadas en /locales
function findLocaledUrlInString(defaultUrl, locale, strings) {
  // buscar defaultUrl en strings.pages.paths y retornar strings.pages.paths.{locale}
  const pagesString = getPageStringByAnyURL(defaultUrl, strings)
  const gettedString = pagesString && pagesString[locale]
  if (!gettedString) {
    // console.warn(`Translation for url '${defaultUrl}' in locale '${locale}' not found. Using default locale instead`)
  }
  return gettedString || defaultUrl
}

// Deduce defaultUrl desde rutas traducidas de páginas alojadas en /locales
function findDefaultUrlInString(localedUrl, locale, strings) {
  const pagesString = getPageStringByAnyURL(localedUrl, strings)
  const gettedString = pagesString[config.defaultLocale]
  if (!gettedString) {
    // console.warn(`Global url for '${localedUrl}' not found in translation for '${locale}'. Using '${localedUrl}' anyway`)
  }
  return gettedString || localedUrl
}

// Obtiene los valores traducidos según cualquier ruta de páginas alojadas en /locales
  // encuentra y retorna el objeto de *definición de la página* según alguna de rutas traducidas
function getPageStringByAnyURL(targetURL, strings) {
  let { paths } = strings[config.defaultLocale].pages;
  return paths && paths.find(pagePaths => {
    return config.locales.find(lang => {
      return pagePaths[lang] === targetURL
    })
  })
}

//////////////////////////////////////
// LOCALIZED URL FOR BLOG ARTICLES
//////////////////////////////////////

// Generar paths de todos los artículos para cada idioma de las categorías selectas
export function getLocalizedPathsOfAllBlogArticlesByCategory (categories) {
  const paths = [];
  // obtener index de las categorias selectas
  categories.forEach(cat => {
    let catIndex = requireBlogCategoryIndexFile(cat)
    // para cada *definición de artículo*
    catIndex.forEach(postDef => {
      // para cada idioma
      config.locales.forEach(locale => {
        // agregar la url al listado de paths
        paths.push({ params: { 
            lang: locale,
            category: cat,
            // obtener la url traducida desde la estructura de datos
            post: postDef[locale]
          } 
        })
      })
    })
  })

  return paths
}


// Obtiene los valores traducidos según cualquier ruta de los post
  // encuentra y retorna un objeto con la definición y contenido del post 
  // según alguna de sus rutas traducidas, 
  // el idioma
  // la categoría
export function getPostContentByLocaledURL (cat, requiredPostPath, locale) {
  let catIndex = requireBlogCategoryIndexFile(cat)
  // en cada definición de post determina si en algún idioma tiene el requiredPostPath
  const postDef = _.find(catIndex, (postDef) => {
    return config.locales.find(lang => {
      return postDef[lang] === requiredPostPath
    })
  })
  // extrae los textos suffijados con locale
  const postDefaultPath = postDef[config.defaultLocale]
  const postContent = getLocalizedBlogPostContent(cat, postDefaultPath, locale)

  return { 
    ...postDef,
    ...postContent,
    lang: locale,
    category: cat
  }
}


function getLocalizedBlogPostContent (cat, postDefaultPath, locale) {
  // extrae los textos suffijados con locale
  const postContentRaw = requireBlogPostContent(cat, postDefaultPath)
  const postContent = {}
  _.forEach(postContentRaw, (value, key) => {
    const suffix = '_' + locale
    // agrega si coincide con el locale selecto
    if (_.includes(key, suffix)) {
      const fieldKey = _.replace(key, suffix, '')
      postContent[fieldKey] = value
    }
    // agrega si no es una ruta traducible
    let includesAny = _.chain(config.locales)
      .map(locale => {
        const suffix = '_' + locale
        return _.includes(key, suffix)
      })
      .reduce((total, value) => total + value, 0)
      .value()
    if (!includesAny) {
      postContent[key] = value
    }
  })
  return postContent
}

/**
 * 
 * @param {String} cat 
 * @param {String} locale 
 * @param {
 *    fullDescription: Boolean,
 *    truncateContentLength: Number
 * } opts
 * @returns
 */
export function getCategoryContentIndex (cat, locale, opts) {
  const catDefIndex = requireBlogCategoryIndexFile(cat)
  const catContentIndex = catDefIndex.map(postDef => {
    const postContent = _.merge(postDef, getLocalizedBlogPostContent(cat, postDef.key || postDef[config.defaultLocale], locale))
    const textContent = _.trim(_.replace(postContent.content, '\n', ''))
    if (opts && opts.fullDescription) {
      postContent.content = textContent;
    }
    else {
      postContent.content = _.truncate(textContent, { 
        length: opts.truncateContentLength || 20
      })
    }
    postContent.category = cat
    return postContent
  })
  return catContentIndex
}