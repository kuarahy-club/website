const fs = require('fs')
const path = require('path')
const { readdirSync, copyFileSync } = require('fs')

const postsDataFolder = './posts'
const publicFolder = './public/blog-files'
mkdirSync(publicFolder)

const getDirectories = source =>
  readdirSync(source, { withFileTypes: true })
  .filter(dirent => dirent.isDirectory())
  .map(dirent => dirent.name)

const getFilesList = source =>
  readdirSync(source, { withFileTypes: true })
  .filter(dirent => !dirent.isDirectory() && dirent.name !== 'index.js')
  .map(dirent => dirent.name)

// copiar assets del blog a la carpeta pública
const postsPath = path.resolve(postsDataFolder)
// recorrer categorías
getDirectories(postsPath).forEach(category => 
  getDirectories(path.resolve(postsPath, category)).forEach(filesFolder => {
    // crear carpeta pública de assets para la categoría
    mkdirSync(path.resolve(publicFolder, category))
    // copiar archivos desde la carpeta files de la categoría
    getFilesList(path.resolve(postsPath, category, 'files')).forEach(fileName => {

      copyFileSync(
        path.resolve(postsPath, category, 'files', fileName),
        path.resolve(publicFolder, category, fileName),
      )
    })
  })
)

function mkdirSync(path) {
  try {
    fs.mkdirSync(path);
  } catch (e) {
    if (e.code != 'EEXIST') throw e;
  }
}
