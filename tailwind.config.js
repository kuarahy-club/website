const defaultColors = require('tailwindcss/colors')
const config = require('./config')

module.exports = {
  mode: 'jit',
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      ...config.colors,
      ...defaultColors,
    },
    extend: {
      fontFamily: {
        'brand-title': ['Spectral SC'],
        'brand-content': ['Spectral']
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
