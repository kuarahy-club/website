module.exports = {
  siteTitle: 'Kuarahy Club',
  googleAnalyticsId: 'G-W7RFWBYSWJ',
  // languages
  defaultLocale: 'es',
  locales: ['es', 'en'],

  colors: {
    'brand-kuarahy': '#6667AB',
    'brand-fundacion': '#769E51',
    'brand-club': '#ED865A',
    'brand-incubadora': '#4E71B7',
    'brand-purple-1': '#3C2C59',
    'brand-purple-2': '#474877',
    'brand-purple-3': '#6667AB',
    'brand-purple-4': '#7C7ED1',
    'brand-purple-5': '#B5B3F9',
    'brand-purple-6': '#DEDDFF',
    'brand-green-1': '#154233',
    'brand-green-2': '#4F6B36',
    'brand-green-3': '#769E51',
    'brand-green-4': '#E4F4EA',
    'brand-orange-1': '#864C32',
    'brand-orange-2': '#BA6946',
    'brand-orange-3': '#ED865A',
    'brand-orange-4': '#F3AB79',
    'brand-orange-5': '#F6E1C6',
    'brand-orange-6': '#F6ECE4',
    'brand-brown-1': '#3E232C',
    'brand-brown-2': '#53363D',
    'brand-brown-3': '#886060',
    'brand-brown-4': '#C09DA5',
    'brand-brown-5': '#D5AAB3',
    'brand-blue-1': '#20304E',
    'brand-blue-2': '#354F82',
    'brand-blue-3': '#4E71B7',
    'brand-blue-4': '#99B3D1',
    'brand-blue-5': '#D9EBFF'
  },

  toTopBtnDuration: 3000,
  scrollYtoShowToTopBtn: 700,
  scrollYtoShowFixedNavbar: 1900,

  blockHeight: 9,

  // escena header
  // templo
  fadeOutTemploStart: 200,
  fadeOutTemploAndLogoEnd: 220,
  fadeOutLogoStart: 145
}
