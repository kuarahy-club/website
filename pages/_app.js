import { useEffect } from 'react'
import { useRouter } from 'next/router'
import '../styles/globals.css'
import withLayout from '../components/withLayout.hoc'
import ReactGA from 'react-ga'
import { googleAnalyticsId } from '../config'

ReactGA.initialize(googleAnalyticsId)
let isInit = false 

export function App({ Component, pageProps }) {
  const router = useRouter()
  
  useEffect(() => {
    const handleRouteChange = (url) => {
      ReactGA.set({ page: location.pathname });
      ReactGA.pageview(location.pathname);
    }
    if (!isInit) {
      isInit = true
      handleRouteChange()
    }
    //When the component is mounted, subscribe to router changes
    //and log those page views
    router.events.on('routeChangeComplete', handleRouteChange)
  
    // If the component is unmounted, unsubscribe
    // from the event with the `off` method
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [router.events])

  return <>
    <Component {...pageProps} />
  </>
}

export default withLayout(App)