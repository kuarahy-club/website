import { useEffect, useState } from 'react'
import {
  motion,
  useSpring,
  useTransform,
  useViewportScroll
} from "framer-motion"
import { bh, vh, vw } from '../lib/animationHelpers'
import { fadeOutTemploStart, fadeOutTemploAndLogoEnd, fadeOutLogoStart } from '../config'

// responsive config
const getAnimationConfig = (item, attr) => {
  const temploLateralSteps = [0, bh(20), bh(35), bh(fadeOutTemploStart), bh(fadeOutTemploAndLogoEnd)]
  // xs
    // templo más separado
    // templo más alto al inicio
  const animatinoConfig = {
    templo: {
      spring: {
        stiffness: 150,
        damping: 30,
      },
      scaleTransform: [
             [0, bh(50), bh(fadeOutTemploStart), bh(fadeOutTemploAndLogoEnd)],
        [vh(20), vh(45), vh(45), vh(65)]
      ],
      topTransform: [
               [0, bh(50), bh(fadeOutTemploStart), bh(fadeOutTemploAndLogoEnd)], 
        [vh(57.5), vh(39), vh(39), vh(20)]
      ],
      lateralTransform: [
               temploLateralSteps, 
        [vw(50.5), vw(52), vw(86), vw(86), vw(105)]
      ]
    }
  }
  if (typeof window === "undefined") { return animatinoConfig[item][attr] }
  
  // landscape ////////////////////////////////////////////
  // xs
    // más pequeño el logo al inicio
    // más arriba el logo al inicio
  // sm
  if (window.innerHeight >= 450) {
    // más grande el logo al inicio
    // más al centro el logo al inicio
  }
  // md
  if (window.innerHeight >= 530) {
    // tamaño y ubicación final del logo
  }
  // normal ////////////////////////////////////////////
  // xs
    // templo más separado
    // templo más alto al inicio
  // xs-sm
  if (window.innerWidth >= 450) {
    // templo un poco menos separado
    animatinoConfig.templo.lateralTransform = [
            temploLateralSteps, 
      [vw(50), vw(52), vw(87), vw(87), vw(105)]
    ]
  }
  // sm
  if (window.innerWidth >= 640) {
    // templo un poco menos separado
    animatinoConfig.templo.lateralTransform = [
            temploLateralSteps, 
      [vw(50.3), vw(52), vw(80), vw(80), vw(105)]
    ]
  }
  // md
  if (window.innerWidth >= 768) {
    // templo un poco menos separado
    animatinoConfig.templo.lateralTransform = [
            temploLateralSteps, 
      [vw(49.5), vw(52), vw(75), vw(75), vw(105)]
    ]
    // templo con imagen más larga
    // templo más bajo al inicio
    animatinoConfig.templo.topTransform = [
          [0, bh(50), bh(fadeOutTemploStart), bh(fadeOutTemploAndLogoEnd)], 
      [vh(57.5), vh(39.5), vh(39.5), vh(20)]
    ]
  }
  // lg
  if (window.innerWidth >= 1024) {
    // templo con separación amplia
    animatinoConfig.templo.lateralTransform = [
          temploLateralSteps, 
      [vw(50), vw(52), vw(70), vw(70), vw(105)]
    ]
  }
  // xl
  if (window.innerWidth >= 1280) {
    animatinoConfig.templo.lateralTransform = [
          temploLateralSteps, 
      [vw(49.3), vw(52), vw(70), vw(70), vw(105)]
    ]
  }
  // 2xl casi (oficialmente empieza en 1536)
  if (window.innerWidth >= 1400) {
    animatinoConfig.templo.lateralTransform = [
          temploLateralSteps, 
      [vw(49.5), vw(52), vw(70), vw(70), vw(105)]
    ]
    animatinoConfig.templo.scaleTransform = [
          [0, bh(50), bh(fadeOutTemploStart), bh(fadeOutTemploAndLogoEnd)],
      [vh(29), vh(45), vh(45), vh(65)]
    ]
    // templo más arriba al inicio
    animatinoConfig.templo.topTransform = [
          [0, bh(50), bh(fadeOutTemploStart), bh(fadeOutTemploAndLogoEnd)], 
      [vh(48.5), vh(39.5), vh(39.5), vh(20)]
    ]
  }

  return animatinoConfig[item][attr]
}

const getTemploImage = (dest) => {
  const imgXs = 'xs'
  const imgMd = 'md'
  const imgLg = 'lg'
  let sizeSelected = imgXs // default
  if (typeof window === "undefined") { return basePath + imgXs }
  if (window.innerWidth >= 490) {
    sizeSelected = imgMd
  }
  if (window.innerWidth >= 768) {
    sizeSelected = imgLg
  }
  if (window.innerWidth >= 1024) {
  }
  return `/assets/escena/${sizeSelected}/templo-${dest}.png`
}

export default function EscenaHeaderTemplo() {
  const [ temploImageIzq, temploImageIzqSet ] = useState()
  const [ temploImageDer, temploImageDerSet ] = useState()
  // Vertical scroll distance in pixels.
  const { scrollY } = useViewportScroll();

  function handleResize() {
    // responsive image
    temploImageIzqSet(getTemploImage('izq'))
    temploImageDerSet(getTemploImage('der'))
  }
  // refresh on resize
  useEffect(() => {
    if (typeof window === "undefined") { return }
    window.addEventListener('resize', handleResize)
    return () => {
      window.removeEventListener('resize', handleResize)
    }
  })
  useEffect(() => {
    handleResize()
  }, [])
  
  // TEMPLO izq ///////////////////////////////////////////////
  const temploIzq = {}
  // scale
  temploIzq.scaleYRange = useTransform(scrollY, ...getAnimationConfig('templo', 'scaleTransform'));
  temploIzq.scaleValue = useSpring(temploIzq.scaleYRange, getAnimationConfig('templo', 'spring'));
  // top
  temploIzq.topYRange = useTransform(scrollY, ...getAnimationConfig('templo', 'topTransform'));
  temploIzq.topValue = useSpring(temploIzq.topYRange, getAnimationConfig('templo', 'spring'));
  // go to left from the right
  temploIzq.rightYRange = useTransform(scrollY, ...getAnimationConfig('templo', 'lateralTransform'));
  temploIzq.rightValue = useSpring(temploIzq.rightYRange, getAnimationConfig('templo', 'spring'));
  ///////////////////////////////////////////////////////

  // TEMPLO der ///////////////////////////////////////////////
  const temploDer = {}
  // scale
  temploDer.scaleYRange = useTransform(scrollY, ...getAnimationConfig('templo', 'scaleTransform'));
  temploDer.scaleValue = useSpring(temploDer.scaleYRange, getAnimationConfig('templo', 'spring'));
  // top
  temploDer.topYRange = useTransform(scrollY, ...getAnimationConfig('templo', 'topTransform'));
  temploDer.topValue = useSpring(temploDer.topYRange, getAnimationConfig('templo', 'spring'));
  // go to right from the left
  temploDer.leftYRange = useTransform(scrollY, ...getAnimationConfig('templo', 'lateralTransform'));
  temploDer.leftValue = useSpring(temploDer.leftYRange, getAnimationConfig('templo', 'spring'));
  ///////////////////////////////////////////////////////

  return (
    <>
      {/* templo => agrandar, subir un poco y mantener estático, 
                    luego de x distancia agrandar y separar todo 
                    dando la sensación de haber entrado al templo */}

      {/* templo-izq => luego del zoom mover a la izquierda casi todo */}
      <motion.div
        className="fixed text-right"
        style={{ 
          width: 3000,
          right: temploIzq.rightValue,
          top: temploIzq.topValue
        }}
      > 
        <motion.img
          src={temploImageIzq}
          style={{
            height: temploIzq.scaleValue,
            display: 'inline'
          }}
        />
      </motion.div>

      {/* templo-der => luego del zoom mover a la derecha casi todo */}
      <motion.div
        className="fixed text-left"
        style={{ 
          width: 3000,
          left: temploDer.leftValue,
          top: temploDer.topValue
        }}
      > 
        <motion.img
          src={temploImageDer}
          style={{
            height: temploDer.scaleValue,
            display: 'inline'
          }}
        />
      </motion.div>
    </>
  )
}