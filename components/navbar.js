import { useContext } from 'react'
import Link from 'next/link'
import { Link as LinkScroll } from 'react-scroll'
import ActiveLink from './activeLink'
import useTranslation from '../lib/translations/useTranslation.hook'
import { LayoutContext } from './layout.context'
import LocaleSwitcher from '../components/localeSwitcher'

export default function NavbarHome({ home, fullMenuHeader, localedPathMap }) {
  const { isNavbarOpen, isScrollUnderOffset, setIsNavbarOpen, setScrollOnInit } = useContext(LayoutContext)
  const { t, locale } = useTranslation()
  const showFullHeader = () => fullMenuHeader || isNavbarOpen || isScrollUnderOffset

  const classes = {
    background: '',
    toggleBtn: 'text-brand-brown-5',
    toggleBtnHover: 'text-brand-brown-4',
    sideMenuMovile: 'bg-brand-kuarahy opacity-90',
    link: 'px-5 py-1 font-normal lg:px-2 lg:text-xs text-brand-orange-6 hover:bg-brand-purple-1 hover:text-brand-orange-6',
    sublink: 'px-5 pl-10 lg:px-4 py-2 font-normal text-brand-orange-6 hover:bg-brand-purple-1 hover:text-brand-orange-6',
    localeSwitcher: ''
  }

  const menuItems = (
    <div className="px-4 w-full font-semibold text-sm">
      <div className="lg:float-left lg:w-4/5 lg:py-3.5 xl:pl-32 2xl:pl-48">
        {home ? (
          <LinkScroll to="header" smooth={true} offset={0} duration={800} href="" className={`menu-item ${classes.link}`}>
            {t('common.menu.inicio')}
          </LinkScroll>
        ) : (
          <ActiveLink href={`/`} activeClass="active" scroll={false} key="homebtn-out">
            <a onClick={() => setScrollOnInit('header')} className={`menu-item ${classes.link}`}>
              {t('common.menu.inicio')}
            </a>
          </ActiveLink>
        )}
        <LinkScroll to="alertHeader" smooth={true} offset={-250} duration={2000} href="" className={`menu-item ${classes.link}`}>
          {t('common.menu.coleccion')}
        </LinkScroll>
        <LinkScroll to="propositoHeader" smooth={true} offset={-350} duration={2000} href="" className={`menu-item ${classes.link}`}>
          {t('common.menu.proposito')}
        </LinkScroll>
        <LinkScroll to="fundacionContent" smooth={true} offset={-130} duration={2000} href="" className={`menu-item ${classes.link}`}>
          {t('common.menu.fundacion')}
        </LinkScroll>
        <LinkScroll to="clubContent" smooth={true} offset={-100} duration={2000} href="" className={`menu-item ${classes.link}`}>
          {t('common.menu.club')}
        </LinkScroll>
        <LinkScroll to="incubadoraContent" smooth={true} offset={-130} duration={2000} href="" className={`menu-item ${classes.link}`}>
          {t('common.menu.incubadora')}
        </LinkScroll>
        <LinkScroll to="staffContent" smooth={true} offset={-130} duration={2000} href="" className={`menu-item ${classes.link}`}>
          {t('common.menu.staff')}
        </LinkScroll>
        <LinkScroll to="roadmapContent" smooth={true} offset={-130} duration={2000} href="" className={`menu-item ${classes.link}`}>
          {t('common.menu.roadmap')}
        </LinkScroll>
        <LinkScroll to="contactContent" smooth={true} offset={-130} duration={2000} href="" className={`menu-item ${classes.link}`}>
          {t('common.menu.contacto')}
        </LinkScroll>
      </div>
      
      <div className="lg:float-right xl:pr-32 2xl:pr-48">
        <LocaleSwitcher
          selectClassName={classes.localeSwitcher}
          optionClassName={classes.link}
          localedPathMap={localedPathMap}
        ></LocaleSwitcher>
      </div>

      <div className="clear-both"></div>
    </div>
  )
  
  return (
    <>
      <header id="header-menu" className={`header-menu z-5000 ${showFullHeader() ? `open ${classes.background}` : `closed`}`}>
        <div className="w-full py-2 lg:hidden">
          {/* navbar header */}
          <div className={`float-left lg:hidden ${showFullHeader() ? 'transition-opacity-full' : 'transition-opacity-none'}`}>
            {/* logo link */}
            <div className="" style={{marginLeft: '50vw'}}>
              <Link href={`/`} scroll={false}>
                <a onClick={() => setScrollOnInit('header')} className="mx-auto inline-block" style={{marginLeft: '-50px'}} title={t('common.site-title')}>
                  <img src={`/assets/kuarahy-logo-white.svg`} style={{width: '80px'}} />
                </a>
              </Link>
            </div>
          </div>
          
          {/* navbar movile trigger */}
          <div className="lg:hidden w-1/5 float-right text-right">
            <button onClick={() => setIsNavbarOpen(!isNavbarOpen)} className="inline-block p-2 pt-5 w-12 text-center text-gray-500 focus:text-gray-700 focus:outline-none" type="button">
              <div className="w-full mx-auto h-auto sm:w-4">
                {isNavbarOpen && (
                  <svg className={`fill-current ${classes.toggleBtn} w-auto h-6 mx-auto`} viewBox="0 0 36.44 130.11">
                    <g id="Capa_2" data-name="Capa 2"><g id="Capa_1-2" data-name="Capa 1"><circle className="cls-1" cx="18.22" cy="18.22" r="18.22"/><circle className="cls-1" cx="18.22" cy="65.06" r="18.22"/><circle className="cls-1" cx="18.22" cy="111.89" r="18.22"/></g></g>
                  </svg>
                )}
                {!isNavbarOpen && (
                  <svg className={`fill-current ${classes.toggleBtnHover} w-auto h-6 mx-auto`} viewBox="0 0 36.44 130.11">
                    <g id="Capa_2" data-name="Capa 2"><g id="Capa_1-2" data-name="Capa 1"><circle className="cls-1" cx="18.22" cy="18.22" r="18.22"/><circle className="cls-1" cx="18.22" cy="65.06" r="18.22"/><circle className="cls-1" cx="18.22" cy="111.89" r="18.22"/></g></g>
                  </svg>
                )}
              </div>
            </button>
          </div>

          <div className="clear-both"></div>
        </div>

        {/* navbar content desktop */}
        <div className={`hidden lg:flex relative w-full mx-auto rounded-full ${(!isScrollUnderOffset && !isNavbarOpen) ? '' : ''}`}>
          {menuItems}
        </div>
      </header>

      {/* navbar content mobile */}
      <div className={`z-4500 fixed top-0 left-0 w-[140px] h-screen ${classes.sideMenuMovile} overflow-y-auto pt-28 pb-4 sm:pt-28 lg:hidden ${isNavbarOpen ? 'block visible' : 'hidden invisible'}`}>
        <div className="mb-6">
          {menuItems}
        </div>
      </div>
    </>
  )
}