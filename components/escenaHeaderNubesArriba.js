import { useEffect, useState } from 'react'
import {
  motion,
  useSpring,
  useTransform,
  useViewportScroll
} from "framer-motion"
import { bh, vh, vw } from '../lib/animationHelpers'
import { fadeOutTemploStart, fadeOutTemploAndLogoEnd, fadeOutLogoStart } from '../config'

// responsive config
const getAnimationConfig = (item, attr) => {
  const nubesLateralSteps = [0, bh(35), bh(fadeOutTemploStart), bh(fadeOutTemploAndLogoEnd)]
  const animatinoConfig = {
    nubes: {
      spring: {
        stiffness: 150,
        damping: 30,
      },
      scaleTransform: [
             [0, bh(50), bh(fadeOutTemploStart), bh(fadeOutTemploAndLogoEnd)],
        [vh(18), vh(14), vh(14), vh(20)]
      ],
      topTransform: [
               [0, bh(50), bh(fadeOutTemploStart), bh(fadeOutTemploAndLogoEnd)], 
        [vh(0), vh(-3), vh(-5), vh(-5)]
      ],
      lateralTransform: [
               nubesLateralSteps, 
        [vw(35.5), vw(50.5), vw(71), vw(105)]
      ]
    }
  }
  if (typeof window === "undefined") { return animatinoConfig[item][attr] }
  
  // landscape ////////////////////////////////////////////
  // xs
  // sm
  if (window.innerHeight >= 450) {}
  // md
  if (window.innerHeight >= 530) {}
  // normal ////////////////////////////////////////////
  // xs
    // nubes más separado
    // nubes más alto al inicio
  // xs-sm
  if (window.innerWidth >= 450) {}
  // sm
  if (window.innerWidth >= 640) {}
  // md
  if (window.innerWidth >= 768) {}
  // lg
  if (window.innerWidth >= 1024) {}
  // xl
  if (window.innerWidth >= 1280) {}
  // 2xl casi (oficialmente empieza en 1536)
  if (window.innerWidth >= 1400) {}

  return animatinoConfig[item][attr]
}

export default function EscenaHeaderNubesArriba() {
  // Vertical scroll distance in pixels.
  const { scrollY } = useViewportScroll();
  
  // nubes izq ///////////////////////////////////////////////
  const nubesIzq = {}
  // scale
  nubesIzq.scaleYRange = useTransform(scrollY, ...getAnimationConfig('nubes', 'scaleTransform'));
  nubesIzq.scaleValue = useSpring(nubesIzq.scaleYRange, getAnimationConfig('nubes', 'spring'));
  // top
  nubesIzq.topYRange = useTransform(scrollY, ...getAnimationConfig('nubes', 'topTransform'));
  nubesIzq.topValue = useSpring(nubesIzq.topYRange, getAnimationConfig('nubes', 'spring'));
  // go to left from the right
  nubesIzq.rightYRange = useTransform(scrollY, ...getAnimationConfig('nubes', 'lateralTransform'));
  nubesIzq.rightValue = useSpring(nubesIzq.rightYRange, getAnimationConfig('nubes', 'spring'));
  ///////////////////////////////////////////////////////
  
  // nubes der ///////////////////////////////////////////////
  const nubesDer = {}
  // scale
  nubesDer.scaleYRange = useTransform(scrollY, ...getAnimationConfig('nubes', 'scaleTransform'));
  nubesDer.scaleValue = useSpring(nubesDer.scaleYRange, getAnimationConfig('nubes', 'spring'));
  // top
  nubesDer.topYRange = useTransform(scrollY, ...getAnimationConfig('nubes', 'topTransform'));
  nubesDer.topValue = useSpring(nubesDer.topYRange, getAnimationConfig('nubes', 'spring'));
  // go to right from the left
  nubesDer.leftYRange = useTransform(scrollY, ...getAnimationConfig('nubes', 'lateralTransform'));
  nubesDer.leftValue = useSpring(nubesDer.leftYRange, getAnimationConfig('nubes', 'spring'));
  ///////////////////////////////////////////////////////

  // noche /////////////////////////////////////////////
  const nubesNoche = {}
  nubesNoche.opacityNocheYRange = useTransform(scrollY, 
      [0, bh(20), bh(20)+1]
    ,[1, 0, 0]
  ,{clamp: false})
  ///////////////////////////////////////////////////////

  return (
    <>
      {/*
        ARRIBA //////////////////////////////////////////////////////////
      */}

      {/* nubes-arriba-izq => luego del zoom mover a la izquierda casi todo */}
      <motion.div
        className="fixed w-full"
        style={{ 
          right: nubesIzq.rightValue,
          top: nubesIzq.topValue
        }}
      > 
        <motion.img
          src={'/assets/escena/xs/nubes-arriba-noche-izq.png'}
          style={{
            height: nubesIzq.scaleValue,
            position: 'absolute',
            zIndex: 16,
            top: 0,
            right: 0,
            opacity: nubesNoche.opacityNocheYRange
          }}
        />
        <motion.img
          src={'/assets/escena/xs/nubes-arriba-dia-izq.png'}
          style={{
            height: nubesIzq.scaleValue,
            position: 'absolute',
            zIndex: 15,     
            top: 0,
            right: 0
          }}
        />
      </motion.div>

      {/* nubes-arriba-der => luego del zoom mover a la derecha casi todo */}
      <motion.div
        className="fixed text-left"
        style={{ 
          width: 400,
          left: nubesDer.leftValue,
          top: nubesDer.topValue
        }}
      > 
        <motion.img
          src={'/assets/escena/xs/nubes-arriba-noche-izq.png'}
          style={{
            height: nubesDer.scaleValue,
            position: 'absolute',
            zIndex: 16,
            top: 0,
            left: 0,
            opacity: nubesNoche.opacityNocheYRange,
            rotateY: 180
          }}
        />
        <motion.img
          src={'/assets/escena/xs/nubes-arriba-dia-izq.png'}
          style={{
            height: nubesDer.scaleValue,
            position: 'absolute',
            zIndex: 15,     
            top: 0,
            left: 0,
            rotateY: 180
          }}
        />
      </motion.div>
    </>
  )
}