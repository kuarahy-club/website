import { useEffect, useContext } from 'react'
import { useAnimation, motion } from "framer-motion"
import { scroller } from 'react-scroll'
import { LayoutContext } from './layout.context'
import map from 'lodash.map'

const containerVariants = {
  visible: {
    opacity: 1,
    scale: 1,
    bottom: 65,
    transition: { 
      duration: 0.7,
      delay: 1.2,
      bottom: { type: 'spring', damping: 10 }
    }
  },

  hidden: { opacity: 0, scale: 0.8, bottom: 0 }
}

const buttonDownVariants = {
  top: { right: -20, opacity: 1, transition: { duration: 0.3, delay: 0.2 } },
  medium: { right: 5, opacity: 1, transition: { duration: 0.3, delay: 0.2 } },
  bottom: { right: 5, opacity: 0, transition: { duration: 0.3, delay: 0.2 } }
}

const buttonUpVariants = {
  top: { opacity: 0, right: -5, transition: { duration: 0.3, delay: 0.2 } },
  medium: { opacity: 1, right: -5, transition: { duration: 0.3, delay: 0.2 } },
  bottom: { opacity: 1, right: 20, transition: { duration: 0.3, delay: 0.2 } }
}

export default function ScrollDown({ activeSection, sectionRefsObj }) {
  const { scrollPosition } = useContext(LayoutContext)

  const containerControls = useAnimation()
  const buttonDownControls = useAnimation()
  const buttonUpControls = useAnimation()

  const sectionObjList = map(sectionRefsObj, section => section)

  useEffect(() => {
    containerControls.start("visible")
  }, []);

  useEffect(() => {
    console.log(activeSection === sectionObjList.length-1, activeSection, sectionObjList.length-1);
    if (scrollPosition < 20 || activeSection === 0) {
      buttonDownControls.start("top")
      buttonUpControls.start("top")
    }
    else if (activeSection === sectionObjList.length-1) {
      buttonDownControls.start("bottom")
      buttonUpControls.start("bottom")
    }
    else {
      buttonDownControls.start("medium")
      buttonUpControls.start("medium")
    }
  }, [scrollPosition]);

  const goTo = (direction) => {
    let selected, opts
    if (direction === 'down') {
      selected = sectionObjList[ activeSection + 1 ] ? sectionObjList[ activeSection + 1 ] : sectionObjList[ activeSection ]
      opts = {
        duration: selected.scroll.durationDown || selected.scroll.duration || 1000,
        delay: 0,
        smooth: selected.scroll.smoothDown || selected.scroll.smooth || true,
        offset: selected.scroll.offsetDown || selected.scroll.offset || 0
      }
    }
    else { // up
      selected = sectionObjList[ activeSection - 1 ] ? sectionObjList[ activeSection - 1 ] : sectionObjList[ activeSection ]
      opts = {
        duration: selected.scroll.durationUp || selected.scroll.duration || 1000,
        delay: 0,
        smooth: selected.scroll.smoothUp || selected.scroll.smooth || true,
        offset: selected.scroll.offsetUp || selected.scroll.offset || 0
      }
    }
    scroller.scrollTo(selected.key, opts)
  }

  return (
    <motion.div
      className='fixed w-full h-0 mx-auto flex place-content-center'
      style={{
        zIndex: 500,
        bottom: 0
      }}
      animate={containerControls}
      initial="hidden"
      variants={containerVariants}
    >
      <motion.button type="button"
        className={`relative p-2 pt-2.5 w-10 h-10 bg-brand-orange-6 border-brand-kuarahy border-2 rounded-full shadow-xl`} 
        animate={buttonDownControls}
        initial="single"
        variants={buttonDownVariants}
        style={{
          zIndex: 520
        }}
        onClick={(e) => {e.stopPropagation(); goTo('down')}}
      >
        <div className="w-full h-full">
          <img src="/assets/elements/control-down.svg" />
        </div>
      </motion.button>

      <motion.button type="button"
        className={`relative p-2 pt-2.5 w-10 h-10 bg-brand-orange-6 border-brand-kuarahy border-2 rounded-full shadow-xl`} 
        animate={buttonUpControls}
        initial="single"
        variants={buttonUpVariants}
        style={{
          zIndex: 510
        }}
        onClick={(e) => {e.stopPropagation(); goTo('up')}}
      >
        <div className="w-full h-full">
          <img src="/assets/elements/control-up.svg" />
        </div>
      </motion.button>
    </motion.div>
  )
}