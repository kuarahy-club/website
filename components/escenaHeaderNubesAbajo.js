import { useEffect, useState } from 'react'
import {
  motion,
  useSpring,
  useTransform,
  useViewportScroll
} from "framer-motion"
import { bh, vh, vw } from '../lib/animationHelpers'
import { fadeOutTemploStart, fadeOutTemploAndLogoEnd, fadeOutLogoStart } from '../config'

// responsive config
const getAnimationConfig = (item, attr) => {
  const nubesLateralSteps = [0, bh(35), bh(fadeOutTemploStart), bh(fadeOutTemploAndLogoEnd)]
  const animatinoConfig = {
    nubes: {
      spring: {
        stiffness: 150,
        damping: 30,
      },
      scaleTransform: [
             [0, bh(50), bh(fadeOutTemploStart), bh(fadeOutTemploAndLogoEnd)],
        [vh(13), vh(10), vh(10), vh(10)]
      ],
      topTransform: [
               [0, bh(50), bh(fadeOutTemploStart), bh(fadeOutTemploAndLogoEnd)], 
        [vh(39), vh(35), vh(35), vh(35)]
      ],
      lateralTransform: [
               nubesLateralSteps, 
        [vw(69.5), vw(90), vw(90), vw(105)]
      ],
      opacityTransform: [
        [0, bh(30)], 
        [1, 0]
      ]
    }
  }
  if (typeof window === "undefined") { return animatinoConfig[item][attr] }
  
  // landscape ////////////////////////////////////////////
  // xs
  // sm
  if (window.innerHeight >= 450) {}
  // md
  if (window.innerHeight >= 530) {}
  // normal ////////////////////////////////////////////
  // xs
    // nubes más separado
    // nubes más alto al inicio
  // xs-sm
  if (window.innerWidth >= 450) {}
  // sm
  if (window.innerWidth >= 640) {}
  // md
  if (window.innerWidth >= 768) {}
  // lg
  if (window.innerWidth >= 1024) {}
  // xl
  if (window.innerWidth >= 1280) {}
  // 2xl casi (oficialmente empieza en 1536)
  if (window.innerWidth >= 1400) {}

  return animatinoConfig[item][attr]
}

const getnubesImage = (variacion) => {
  const imgXs = 'xs'
  const imgMd = 'xs'
  const imgLg = 'lg'
  let sizeSelected = imgXs // default
  if (typeof window === "undefined") { return basePath + imgXs }
  if (window.innerWidth >= 490) {
    sizeSelected = imgMd
  }
  if (window.innerWidth >= 768) {
    sizeSelected = imgLg
  }
  if (window.innerWidth >= 1024) {
  }
  return `/assets/escena/${sizeSelected}/nubes-${variacion}-izq.png`
}

export default function EscenaHeaderNubesAbajo() {
  const [ nubesImageArriba, nubesImageArribaSet ] = useState()
  const [ nubesImageDer, nubesImageDerSet ] = useState()
  // Vertical scroll distance in pixels.
  const { scrollY } = useViewportScroll();

  function handleResize() {
    // responsive image
    nubesImageArribaSet(getnubesImage('abajo-noche'))
    // nubesImageDerSet(getnubesImage('abajo-noche'))
  }
  // refresh on resize
  useEffect(() => {
    if (typeof window === "undefined") { return }
    window.addEventListener('resize', handleResize)
    return () => {
      window.removeEventListener('resize', handleResize)
    }
  })
  useEffect(() => {
    handleResize()
  }, [])
  
  // nubes izq ///////////////////////////////////////////////
  const nubesIzq = {}
  // scale
  nubesIzq.scaleYRange = useTransform(scrollY, ...getAnimationConfig('nubes', 'scaleTransform'));
  nubesIzq.scaleValue = useSpring(nubesIzq.scaleYRange, getAnimationConfig('nubes', 'spring'));
  // top
  nubesIzq.topYRange = useTransform(scrollY, ...getAnimationConfig('nubes', 'topTransform'));
  nubesIzq.topValue = useSpring(nubesIzq.topYRange, getAnimationConfig('nubes', 'spring'));
  // go to left from the right
  nubesIzq.rightYRange = useTransform(scrollY, ...getAnimationConfig('nubes', 'lateralTransform'));
  nubesIzq.rightValue = useSpring(nubesIzq.rightYRange, getAnimationConfig('nubes', 'spring'));
  ///////////////////////////////////////////////////////

  // nubes der ///////////////////////////////////////////////
  const nubesDer = {}
  // scale
  nubesDer.scaleYRange = useTransform(scrollY, ...getAnimationConfig('nubes', 'scaleTransform'));
  nubesDer.scaleValue = useSpring(nubesDer.scaleYRange, getAnimationConfig('nubes', 'spring'));
  // top
  nubesDer.topYRange = useTransform(scrollY, ...getAnimationConfig('nubes', 'topTransform'));
  nubesDer.topValue = useSpring(nubesDer.topYRange, getAnimationConfig('nubes', 'spring'));
  // go to right from the left
  nubesDer.leftYRange = useTransform(scrollY, ...getAnimationConfig('nubes', 'lateralTransform'));
  nubesDer.leftValue = useSpring(nubesDer.leftYRange, getAnimationConfig('nubes', 'spring'));
  ///////////////////////////////////////////////////////

  const nubes = {}
  // common //////////////////////////////////////////////////
  nubes.opacityYRange = useTransform(scrollY, ...getAnimationConfig('nubes', 'opacityTransform'));
  nubes.opacityValue = useSpring(nubes.opacityYRange, getAnimationConfig('nubes', 'spring'));
  ///////////////////////////////////////////////////////

  return (
    <>
      {/*
        ARRIBA //////////////////////////////////////////////////////////
      */}

      {/* nubes-arriba-izq => luego del zoom mover a la izquierda casi todo */}
      <motion.div
        className="fixed text-right"
        style={{ 
          width: 3000,
          right: nubesIzq.rightValue,
          top: nubesIzq.topValue
        }}
      > 
        <motion.img
          src={nubesImageArriba}
          style={{
            height: nubesIzq.scaleValue,
            opacity: nubes.opacityValue,
            display: 'inline'
          }}
        />
      </motion.div>

      {/* nubes-arriba-der => luego del zoom mover a la derecha casi todo */}
      <motion.div
        className="fixed text-left"
        style={{ 
          width: 3000,
          left: nubesDer.leftValue,
          top: nubesDer.topValue
        }}
      > 
        <motion.img
          src={nubesImageArriba}
          style={{
            height: nubesDer.scaleValue,
            opacity: nubes.opacityValue,
            display: 'inline',
            rotateY: 180,
            // rotateX: 180
          }}
        />
      </motion.div>
    </>
  )
}