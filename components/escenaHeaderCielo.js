import { useEffect, useState } from 'react'
import {
  motion,
  useSpring,
  useTransform,
  useViewportScroll
} from "framer-motion"
import Link from 'next/link'
import { bh, vh, vw } from '../lib/animationHelpers'
import { fadeOutTemploStart, fadeOutTemploAndLogoEnd, fadeOutLogoStart } from '../config'
import useTranslation from '../lib/translations/useTranslation.hook'

// responsive config
const getAnimationConfig = (item, attr) => {
  
}

const getImage = (dest) => {
  // const imgXs = 'xs'
  // const imgMd = 'md'
  const imgLg = 'xs'
  let sizeSelected = imgLg // default
  if (typeof window === "undefined") { return basePath + imgXs }
  // if (window.innerWidth >= 490) {
  //   sizeSelected = imgMd
  // }
  // if (window.innerWidth >= 768) {
  //   sizeSelected = imgLg
  // }
  // if (window.innerWidth >= 1024) {
  // }
  return `/assets/escena/${sizeSelected}/cielo-dia.jpg`
}

export default function EscenaHeaderCielo() {
  const { t, locale } = useTranslation()
  // Vertical scroll distance in pixels.
  const { scrollY } = useViewportScroll();

  // LOGO ///////////////////////////////////////////////
  const cielo = {}
  // opacity noche
  cielo.opacityNocheYRange = useTransform(scrollY, 
      [0, bh(20), bh(20)+1]
    ,[1, 0, 0]
  ,{clamp: false})
  // opacity dia
  cielo.opacityDiaYRange = useTransform(scrollY, 
      [0, bh(fadeOutLogoStart), bh(fadeOutTemploAndLogoEnd), bh(fadeOutTemploAndLogoEnd)+1]
    ,[1, 1, 0, 0]
  ,{clamp: false})

  cielo.positionYRange = useTransform(scrollY,
    [0, bh(fadeOutTemploAndLogoEnd)-1, bh(fadeOutTemploAndLogoEnd), bh(fadeOutTemploAndLogoEnd)+1],
    [0, 0, -3000, -3000]
  )
  ///////////////////////////////////////////////////////

  return (
    <>
      {/* cielo => achicar */}
      <motion.div
        className="w-full fixed"
        style={{
          y: cielo.positionYRange,
        }}
      > 
        <motion.img
          src={`/assets/escena/xs/cielo-noche.jpg`} className="w-full"
          style={{
            opacity: cielo.opacityNocheYRange,
            position: 'absolute',
            zIndex: 11,
            height: '120vh'
          }}
        />
        <motion.img
          src={`/assets/escena/xs/cielo-dia.jpg`} className="w-full"
          style={{
            opacity: cielo.opacityDiaYRange,
            position: 'absolute',
            zIndex: 10,
            height: '110vh',
            top: '-10vh'
          }}
        />
      </motion.div>
    </>
  )
}