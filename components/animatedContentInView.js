import { useEffect } from 'react'
import { useAnimation, motion } from "framer-motion"
import { useInView } from "react-intersection-observer"

const contentVariants = {
  visible: { opacity: 1, scale: 1, transition: { duration: 0.5 } },
  hidden: { opacity: 0, scale: 0.9 }
}

export default function AnimatedContentInView({ children, render, slowOut, className, getTopOffset, debug }) {
  const controls = useAnimation()
  const [ref, inView] = useInView()

  useEffect(() => {
    if (inView) {
      controls.start("visible");
    }
  }, [controls, inView]);

  return (
    
    <motion.div 
      ref={ref}
      animate={controls}
      initial="hidden"
      variants={contentVariants}
    >
      {children}
    </motion.div>
  )
} 