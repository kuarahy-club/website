import { useEffect, useState } from 'react'
import {
  m,
  motion,
  useSpring,
  useTransform,
  useViewportScroll
} from "framer-motion"
import { bh, vh, vw } from '../lib/animationHelpers'
import { fadeOutTemploStart, fadeOutTemploAndLogoEnd, fadeOutLogoStart } from '../config'

// responsive config
const getAnimationConfig = (item, attr) => {
  const animatinoConfig = {
    vegetacionNoche: {
      spring: {
        stiffness: 150,
        damping: 30,
      },
      topTransform: [
               [0, bh(30), bh(35)+1], 
        [vh(50), vh(80), vh(80)]
      ],
      opacityTransform: [
          [0, bh(20), bh(20)+1], 
        [1, 0, 0]
      ]
    },
    vegetacion: {
      spring: {
        stiffness: 150,
        damping: 30,
      },
      scaleTransform: [
             [0, bh(50), bh(fadeOutTemploStart), bh(fadeOutTemploAndLogoEnd)],
        [vh(35), vh(50), vh(50), vh(50)]
      ],
      topTransform: [
               [0, bh(50), bh(fadeOutTemploStart), bh(fadeOutTemploAndLogoEnd)], 
        [vh(50), vh(35), vh(35), vh(35)]
      ],
      lateralTransform: [
            [0, bh(35), bh(fadeOutTemploStart), bh(fadeOutTemploAndLogoEnd)], 
        [vw(80), vw(54), vw(54), vw(105)]
      ],
      opacityTransform: [
          [0, bh(20), bh(20)+1], 
        [0, 1, 1]
      ]
    }
  }
  if (typeof window === "undefined") { return animatinoConfig[item][attr] }
  
  // landscape ////////////////////////////////////////////
  // xs
  // sm
  if (window.innerHeight >= 450) {}
  // md
  if (window.innerHeight >= 530) {}
  // normal ////////////////////////////////////////////
  // xs
    // vegetacion más separado
    // vegetacion más alto al inicio
  // xs-sm
  if (window.innerWidth >= 450) {}
  // sm
  if (window.innerWidth >= 640) {}
  // md
  if (window.innerWidth >= 768) {}
  // lg
  if (window.innerWidth >= 1024) {}
  // xl
  if (window.innerWidth >= 1280) {}
  // 2xl casi (oficialmente empieza en 1536)
  if (window.innerWidth >= 1400) {}

  return animatinoConfig[item][attr]
}

const getVegetacionImage = (dest) => {
  const imgXs = 'xs'
  const imgMd = 'xs'
  const imgLg = 'lg'
  let sizeSelected = imgXs // default
  if (typeof window === "undefined") { return basePath + imgXs }
  if (window.innerWidth >= 490) {
    sizeSelected = imgMd
  }
  if (window.innerWidth >= 768) {
    sizeSelected = imgLg
  }
  if (window.innerWidth >= 1024) {
  }
  return `/assets/escena/${sizeSelected}/vegetacion-${dest}.png`
}

const getVegetacionNocheImage = () => {
  const imgXs = 'xs'
  const imgMd = 'xs'
  const imgLg = 'lg'
  let sizeSelected = imgXs // default
  if (typeof window === "undefined") { return basePath + imgXs }
  if (window.innerWidth >= 490) {
    sizeSelected = imgMd
  }
  if (window.innerWidth >= 768) {
    sizeSelected = imgLg
  }
  if (window.innerWidth >= 1024) {
  }
  return `/assets/escena/${sizeSelected}/vegetacion-noche.png`
}

export default function EscenaHeadervegetacion() {
  const [ vegetacionImageIzq, vegetacionImageIzqSet ] = useState()
  const [ vegetacionNocheImage, vegetacionNocheImageSet ] = useState()
  // Vertical scroll distance in pixels.
  const { scrollY } = useViewportScroll();

  function handleResize() {
    // responsive image
    vegetacionImageIzqSet(getVegetacionImage('dia-izq'))
    vegetacionNocheImageSet(getVegetacionNocheImage())
  }
  // refresh on resize
  useEffect(() => {
    if (typeof window === "undefined") { return }
    window.addEventListener('resize', handleResize)
    return () => {
      window.removeEventListener('resize', handleResize)
    }
  })
  useEffect(() => {
    handleResize()
  }, [])
  
  // vegetacionNoche der ///////////////////////////////////////////////
  const vegetacionNoche = {}
  // top
  vegetacionNoche.topYRange = useTransform(scrollY, ...getAnimationConfig('vegetacionNoche', 'topTransform'));
  vegetacionNoche.topValue = useSpring(vegetacionNoche.topYRange, getAnimationConfig('vegetacionNoche', 'spring'));
  // opacity
  vegetacionNoche.opacityYRange = useTransform(scrollY, ...getAnimationConfig('vegetacionNoche', 'opacityTransform'));
  ///////////////////////////////////////////////////////

  // vegetacion izq ///////////////////////////////////////////////
  const vegetacionDiaIzq = {}
  // scale
  vegetacionDiaIzq.scaleYRange = useTransform(scrollY, ...getAnimationConfig('vegetacion', 'scaleTransform'));
  vegetacionDiaIzq.scaleValue = useSpring(vegetacionDiaIzq.scaleYRange, getAnimationConfig('vegetacion', 'spring'));
  // top
  vegetacionDiaIzq.topYRange = useTransform(scrollY, ...getAnimationConfig('vegetacion', 'topTransform'));
  vegetacionDiaIzq.topValue = useSpring(vegetacionDiaIzq.topYRange, getAnimationConfig('vegetacion', 'spring'));
  // go to left from the right
  vegetacionDiaIzq.rightYRange = useTransform(scrollY, ...getAnimationConfig('vegetacion', 'lateralTransform'));
  vegetacionDiaIzq.rightValue = useSpring(vegetacionDiaIzq.rightYRange, getAnimationConfig('vegetacion', 'spring'));
  ///////////////////////////////////////////////////////

  // vegetacion der ///////////////////////////////////////////////
  const vegetacionDiaDer = {}
  // scale
  vegetacionDiaDer.scaleYRange = useTransform(scrollY, ...getAnimationConfig('vegetacion', 'scaleTransform'));
  vegetacionDiaDer.scaleValue = useSpring(vegetacionDiaDer.scaleYRange, getAnimationConfig('vegetacion', 'spring'));
  // top
  vegetacionDiaDer.topYRange = useTransform(scrollY, ...getAnimationConfig('vegetacion', 'topTransform'));
  vegetacionDiaDer.topValue = useSpring(vegetacionDiaDer.topYRange, getAnimationConfig('vegetacion', 'spring'));
  // go to right from the left
  vegetacionDiaDer.leftYRange = useTransform(scrollY, ...getAnimationConfig('vegetacion', 'lateralTransform'));
  vegetacionDiaDer.leftValue = useSpring(vegetacionDiaDer.leftYRange, getAnimationConfig('vegetacion', 'spring'));
  ///////////////////////////////////////////////////////

  // vegetacion dia opacity
  const vegetacionDia = {}
  vegetacionDia.opacityYRange = useTransform(scrollY, ...getAnimationConfig('vegetacion', 'opacityTransform'));

  return (
    <>
      {/* vegetacionNoche */}
      <motion.div
        className="fixed text-center"
        style={{ 
          width: '100%',
          top: vegetacionNoche.topValue
        }}
      > 
        <motion.img
          src={vegetacionNocheImage}
          style={{
            opacity: vegetacionNoche.opacityYRange,
            display: 'inline'
          }}
        />
      </motion.div>
      {/* vegetacion => agrandar, subir un poco y mantener estático, 
                    luego de x distancia agrandar y separar todo 
                    dando la sensación de haber entrado al vegetacion */}

      {/* vegetacion-izq => luego del zoom mover a la izquierda casi todo */}
      <motion.div
        className="fixed text-right"
        style={{ 
          width: 3000,
          right: vegetacionDiaIzq.rightValue,
          top: vegetacionDiaIzq.topValue
        }}
      > 
        <motion.img
          src={vegetacionImageIzq}
          style={{
            height: vegetacionDiaIzq.scaleValue,
            opacity: vegetacionDia.opacityYRange,
            display: 'inline'
          }}
        />
      </motion.div>

      {/* vegetacion-der => luego del zoom mover a la derecha casi todo */}
      <motion.div
        className="fixed text-left"
        style={{ 
          width: 3000,
          left: vegetacionDiaDer.leftValue,
          top: vegetacionDiaDer.topValue
        }}
      > 
        <motion.img
          src={vegetacionImageIzq}
          style={{
            height: vegetacionDiaDer.scaleValue,
            opacity: vegetacionDia.opacityYRange,
            display: 'inline',
            rotateY: 180
          }}
        />
      </motion.div>
    </>
  )
}