import { useEffect, useState } from 'react'
import {
  motion,
  useSpring,
  useTransform,
  useViewportScroll
} from "framer-motion"
import { bh, vh, vw } from '../lib/animationHelpers'
import { fadeOutTemploStart, fadeOutTemploAndLogoEnd, fadeOutLogoStart } from '../config'

// responsive config
const getAnimationConfig = (item, attr) => {
  const pastizalLateralSteps = [0, bh(20), bh(35), bh(fadeOutTemploStart), bh(fadeOutTemploAndLogoEnd)]
  const animatinoConfig = {
    camino: {
      spring: {
        stiffness: 150,
        damping: 30,
      },
      scaleTransform: [
             [0, bh(50), bh(fadeOutTemploStart), bh(fadeOutTemploAndLogoEnd)],
        [vh(30), vh(35), vh(35), vh(40)]
      ],
      topTransform: [
               [0, bh(50), bh(fadeOutTemploStart), bh(fadeOutTemploAndLogoEnd)], 
        [vh(77), vh(83), vh(83), vh(110)]
      ]
    },
    pastizal: {
      spring: {
        stiffness: 150,
        damping: 30,
      },
      scaleTransform: [
             [0, bh(50), bh(fadeOutTemploStart), bh(fadeOutTemploAndLogoEnd)],
        [vh(30), vh(35), vh(35), vh(40)]
      ],
      topTransform: [
               [0, bh(50), bh(fadeOutTemploStart), bh(fadeOutTemploAndLogoEnd)], 
        [vh(70), vh(75), vh(75), vh(75)]
      ],
      lateralTransform: [
               pastizalLateralSteps, 
        [vw(49), vw(51), vw(65), vw(65), vw(105)]
      ]
    }
  }
  if (typeof window === "undefined") { return animatinoConfig[item][attr] }
  
  // landscape ////////////////////////////////////////////
  // xs
  // sm
  if (window.innerHeight >= 450) {}
  // md
  if (window.innerHeight >= 530) {}
  // normal ////////////////////////////////////////////
  // xs
    // pastizal más separado
    // pastizal más alto al inicio
  // xs-sm
  if (window.innerWidth >= 450) {}
  // sm
  if (window.innerWidth >= 640) {}
  // md
  if (window.innerWidth >= 768) {}
  // lg
  if (window.innerWidth >= 1024) {}
  // xl
  if (window.innerWidth >= 1280) {
    animatinoConfig.pastizal.lateralTransform = [
          pastizalLateralSteps, 
      [vw(49.3), vw(51), vw(65), vw(65), vw(105)]
    ]
  }
  // 2xl casi (oficialmente empieza en 1536)
  if (window.innerWidth >= 1400) {}

  return animatinoConfig[item][attr]
}

const getPastizalImage = (dest) => {
  const imgXs = 'xs'
  const imgMd = 'md'
  const imgLg = 'lg'
  let sizeSelected = imgXs // default
  if (typeof window === "undefined") { return basePath + imgXs }
  if (window.innerWidth >= 490) {
    sizeSelected = imgMd
  }
  if (window.innerWidth >= 768) {
    sizeSelected = imgLg
  }
  if (window.innerWidth >= 1024) {
  }
  return `/assets/escena/${sizeSelected}/pastizal-${dest}.png`
}

export default function EscenaHeaderpastizal() {
  const [ pastizalImageIzq, pastizalImageIzqSet ] = useState()
  const [ pastizalImageDer, pastizalImageDerSet ] = useState()
  // Vertical scroll distance in pixels.
  const { scrollY } = useViewportScroll();

  function handleResize() {
    // responsive image
    pastizalImageIzqSet(getPastizalImage('izq'))
    pastizalImageDerSet(getPastizalImage('der'))
  }
  // refresh on resize
  useEffect(() => {
    if (typeof window === "undefined") { return }
    window.addEventListener('resize', handleResize)
    return () => {
      window.removeEventListener('resize', handleResize)
    }
  })
  useEffect(() => {
    handleResize()
  }, [])
  
  // camino der ///////////////////////////////////////////////
  const camino = {}
  // scale
  camino.scaleYRange = useTransform(scrollY, ...getAnimationConfig('camino', 'scaleTransform'));
  camino.scaleValue = useSpring(camino.scaleYRange, getAnimationConfig('camino', 'spring'));
  // top
  camino.topYRange = useTransform(scrollY, ...getAnimationConfig('camino', 'topTransform'));
  camino.topValue = useSpring(camino.topYRange, getAnimationConfig('camino', 'spring'));
  // opacity noche
  camino.opacityNocheYRange = useTransform(scrollY, 
      [0, bh(fadeOutLogoStart), bh(fadeOutTemploAndLogoEnd), bh(fadeOutTemploAndLogoEnd)+1]
    ,[1, 1, 0, 0]
  ,{clamp: false})
  ///////////////////////////////////////////////////////

  // pastizal izq ///////////////////////////////////////////////
  const pastizalIzq = {}
  // scale
  pastizalIzq.scaleYRange = useTransform(scrollY, ...getAnimationConfig('pastizal', 'scaleTransform'));
  pastizalIzq.scaleValue = useSpring(pastizalIzq.scaleYRange, getAnimationConfig('pastizal', 'spring'));
  // top
  pastizalIzq.topYRange = useTransform(scrollY, ...getAnimationConfig('pastizal', 'topTransform'));
  pastizalIzq.topValue = useSpring(pastizalIzq.topYRange, getAnimationConfig('pastizal', 'spring'));
  // go to left from the right
  pastizalIzq.rightYRange = useTransform(scrollY, ...getAnimationConfig('pastizal', 'lateralTransform'));
  pastizalIzq.rightValue = useSpring(pastizalIzq.rightYRange, getAnimationConfig('pastizal', 'spring'));
  ///////////////////////////////////////////////////////

  // pastizal der ///////////////////////////////////////////////
  const pastizalDer = {}
  // scale
  pastizalDer.scaleYRange = useTransform(scrollY, ...getAnimationConfig('pastizal', 'scaleTransform'));
  pastizalDer.scaleValue = useSpring(pastizalDer.scaleYRange, getAnimationConfig('pastizal', 'spring'));
  // top
  pastizalDer.topYRange = useTransform(scrollY, ...getAnimationConfig('pastizal', 'topTransform'));
  pastizalDer.topValue = useSpring(pastizalDer.topYRange, getAnimationConfig('pastizal', 'spring'));
  // go to right from the left
  pastizalDer.leftYRange = useTransform(scrollY, ...getAnimationConfig('pastizal', 'lateralTransform'));
  pastizalDer.leftValue = useSpring(pastizalDer.leftYRange, getAnimationConfig('pastizal', 'spring'));
  ///////////////////////////////////////////////////////

  return (
    <>
      {/* camino */}
      <motion.div
        className="fixed text-center"
        style={{ 
          width: '100%',
          top: camino.topValue
        }}
      > 
        <motion.img
          src={'/assets/escena/xs/pastizal-camino.png'}
          style={{
            height: camino.scaleValue,
            opacity: camino.opacityNocheYRange,
            display: 'inline'
          }}
        />
      </motion.div>
      {/* pastizal => agrandar, subir un poco y mantener estático, 
                    luego de x distancia agrandar y separar todo 
                    dando la sensación de haber entrado al pastizal */}

      {/* pastizal-izq => luego del zoom mover a la izquierda casi todo */}
      <motion.div
        className="fixed text-right"
        style={{ 
          width: 3000,
          right: pastizalIzq.rightValue,
          top: pastizalIzq.topValue
        }}
      > 
        <motion.img
          src={pastizalImageIzq}
          style={{
            height: pastizalIzq.scaleValue,
            display: 'inline'
          }}
        />
      </motion.div>

      {/* pastizal-der => luego del zoom mover a la derecha casi todo */}
      <motion.div
        className="fixed text-left"
        style={{ 
          width: 3000,
          left: pastizalDer.leftValue,
          top: pastizalDer.topValue
        }}
      > 
        <motion.img
          src={pastizalImageDer}
          style={{
            height: pastizalDer.scaleValue,
            display: 'inline'
          }}
        />
      </motion.div>
    </>
  )
}